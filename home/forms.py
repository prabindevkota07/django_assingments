from django import forms
from django.core.exceptions import ValidationError
from .models import Aboutus


class About(forms.ModelForm):
    class Meta:
        model = Aboutus
        fields = '__all__'

    # title = forms.CharField(max_length=122, required=True, label="title")
    # description = forms.CharField(widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(About, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
            self.fields['title'].widget.attrs.update({'placeholder': 'Enter Title'})

    def clean(self):
        cleaned_data = super().clean()
        title = self.cleaned_data.get('title')
        for data in Aboutus.objects.all():
            if title == data.title:
                raise ValidationError("Title Already Exists")
        return cleaned_data
