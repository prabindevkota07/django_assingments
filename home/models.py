from django.db import models


# Create your models here.
class Contact(models.Model):
    email = models.CharField(max_length=122, default="")
    message = models.TextField(default="")

    def __str__(self):
        return self.email


class Aboutus(models.Model):
    title = models.CharField(max_length=122)
    description = models.TextField()

    def __str__(self):
        return self.title
