from django.contrib import admin
from django.urls import path
from . import views
from .views import ExpenseList, AboutView


urlpatterns = [
    path("", views.index, name='home'),
    path('project', views.project, name='project'),
    path('services', views.services, name='services'),
    path('contact', views.contact, name='contact'),
    path('about', AboutView.as_view()),
    path('expense', ExpenseList.as_view()),
    path('delete/<id>', views.AboutView.delete, name='delete'),
    path('update/<id>', views.AboutView.update, name='update'),
]