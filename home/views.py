from django.shortcuts import render, HttpResponse, get_object_or_404, redirect
from django.views import generic
from .models import *
from django.views.generic import View, FormView, RedirectView, DeleteView
from .forms import *
from django.core.exceptions import ValidationError
from django.contrib import messages


# Create your views here.
def index(request):
    # return HttpResponse("this is homepage")
    context = {
        'name': "Prabin Devkota"
    }
    if request.method == "POST":

        return render(request, 'index.html', context)
    else:

        return render(request, 'index.html', context)


def project(request):
    # return HttpResponse("this is project page")

    student_list = [
        {
            "id": 1,
            "name": "Ram Maharjan",
            "roll": 1,
            "address": "Gangabu",
            "phone": 980000000000

        },
        {
            "id": 2,
            "name": "Prabin Devkota",
            "roll": 2,
            "address": "Baniyatar",
            "phone": 9800123456

        },
        {
            "id": 3,
            "name": "Sanil lama",
            "roll": 3,
            "address": "Phutung",
            "phone": 980000000023

        },
        {
            "id": 4,
            "name": "Ram Maharjan",
            "roll": 4,
            "address": "Gangabu",
            "phone": 980000000000

        },
        {
            "id": 5,
            "name": "Ram Maharjan",
            "roll": 5,
            "address": "Gangabu",
            "phone": 980000000000

        }, {
            "id": 6,
            "name": "Ram Maharjan",
            "roll": 6,
            "address": "Gangabu",
            "phone": 980000000000

        },
        {
            "id": 7,
            "name": "Ram Maharjan",
            "roll": 7,
            "address": "Gangabu",
            "phone": 980000000000

        },
        {
            "id": 8,
            "name": "Ram Maharjan",
            "roll": 8,
            "address": "Gangabu",
            "phone": 980000000000

        },
        {
            "id": 9,
            "name": "Ram Maharjan",
            "roll": 9,
            "address": "Gangabu",
            "phone": 980000000000

        }, {
            "id": 10,
            "name": "Ram Maharjan",
            "roll": 10,
            "address": "Gangabu",
            "phone": 980000000000

        },
    ]
    context = {
        'student_list': student_list
    }

    return render(request, 'project.html', context)


def services(request):
    # return HttpResponse("this is services page")
    return render(request, 'services.html')


def contact(request):
    # return HttpResponse("this is contact page")
    enq = Contact.objects.all()
    if request.method == "POST":
        email = request.POST.get('email')
        message = request.POST.get('message')
        for data in enq:
            if data.email == email:
                messages.error(request, 'Email Already exists.')
                return render(request, 'contact.html', {'enq': enq})

        contact = Contact(email=email, message=message)
        contact.save()
        enq = Contact.objects.all()
        messages.success(request, 'Success')
        return render(request, 'contact.html', {'enq': enq})
    context = {
        'enq': enq
    }
    return render(request, 'contact.html', context, )


class AboutView(FormView):
    form_class = About
    template_name = 'about.html'

    def form_invalid(self, form):
        details = Aboutus.objects.all()
        context = {
            'details': details,
            'form': form
        }
        return render(self.request, 'about.html', context)

    def form_valid(self, form):
        details = Aboutus.objects.all()
        form.save()
        eform = About()
        context = {
            'details': details,
            'form': eform
        }
        messages.success(self.request, 'Success')
        return render(self.request, 'about.html', context)

    def get(self, request):
        details = Aboutus.objects.all()
        form = About()
        context = {
            'details': details,
            'form': form
        }

        return render(self.request, 'about.html', context)

    def delete(request, id):
        delete_data = Aboutus.objects.get(pk=id)
        delete_data.delete()
        return redirect('/about')

    def update(request, id):
        update_data = Aboutus.objects.get(pk=id)
        if request.method == 'GET':
            uform = About(instance=update_data)
            details = Aboutus.objects.all()
            context = {
                'details': details,
                'form': uform
            }
            return render(request, 'update.html', context)
        if request.method == "POST":
            uform = About(request.POST, instance=update_data)
            if uform.is_valid():
                uform.save()
                details = Aboutus.objects.all()
                form = About
                context = {
                'details': details,
                'form': form
                }
                return render(request, 'about.html', context)
            else:
                details = Aboutus.objects.all()
                context = {
                    'details': details,
                    'form': uform
                }
                return render(request, 'update.html', context)

class ExpenseList(View):
    def get(self, request):
        expense = [
            {
                'title': "Movie"
            },
            {
                'title': "test1"
            }
        ]
        context = {
            "expense": expense
        }
        return render(request, 'expense.html', context)
